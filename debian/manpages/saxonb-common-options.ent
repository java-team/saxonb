<?xml version="1.0" encoding="UTF-8"?>

<para>A summary of supported options is included below. Options can be passed
  in any order.</para>

<para>We first report the options which are shared by
  <command>&dhpackage;</command> and <command>&dhother;</command>, then those
  specific to <command>&dhpackage;</command> (or which have a different
  semantics).</para>

<para>Some options are flagged as "Saxon-SA only", such options work only if
  Saxon-SA is installed. Note that Saxon-SA is a commercial product, as such it
  is not available in Debian.</para>

<refsect2>
  <title>Common options</title>

  <variablelist>

    <varlistentry>
      <term><option>-cr</option>:<replaceable>classname</replaceable></term>
      <listitem>
	<para>Specify a class to be used for processing collection URIs passed
	  to the <function>collection()</function> function. The class must
	  implement
	  <classname>net.sf.saxon.CollectionURIResolver</classname>.</para>
      </listitem>
    </varlistentry>

    <varlistentry>
      <term><option>-dtd</option>:<group choice="req"><arg>on</arg><arg>off</arg></group></term>
      <listitem>
	<para>Enable or disable DTD validation. Default: off.</para>
      </listitem>
    </varlistentry>

    <varlistentry>
      <term><option>-expand</option>:<group choice="req"><arg>on</arg><arg>off</arg></group></term>
      <listitem>
	<para>When validation is enabled, expand default values in validated
	  documents. This option enables or disables such an expansion.
	  Default: on.</para>
      </listitem>
    </varlistentry>

    <varlistentry>
      <term><option>-explain</option><arg choice="opt">:<replaceable>filename</replaceable></arg></term>
      <listitem>
	<para>Display an execution plan; the output is in XML format.  If
	  filename is not given it will be displayed on standard
	  output.</para>
      </listitem>
    </varlistentry>

    <varlistentry>
      <term><option>-ext</option>:<group choice="req"><arg>on</arg><arg>off</arg></group></term>
      <listitem>
	<para>Enable or disable the ability to invoke external Java functions
	  from query files and stylesheets. Beware that enabling them is a
	  potential security risk when processing untrusted files. Default:
	  off.</para>
      </listitem>
    </varlistentry>

    <varlistentry>
      <term><option>-l</option>:<group choice="req"><arg>on</arg><arg>off</arg></group></term>
      <listitem>
	<para>Keep (when on) or throw away (when off) line numbers in tress
	  corresponding to source documents. When kept, line numbers can be
	  accessed using the function
	  <function>saxon:line-number()</function>. Default: off.</para>
      </listitem>
    </varlistentry>

    <varlistentry>
      <term><option>-outval</option>:<group choice="req"><arg>recover</arg><arg>fatal</arg></group></term>
      <listitem>
	<para>When validation is required, set whether validation errors are
	  fatal (when "fatal" is passed) or if they only trigger warnings (when
	  "recover" is). Default: fatal.</para>
      </listitem>
    </varlistentry>

    <varlistentry>
      <term><option>-p</option>:<group choice="req"><arg>on</arg><arg>off</arg></group></term>
      <listitem>
	<para> Enable or disable usage of the
	  <classname>PTreeURIResolver</classname>. Saxon-SA only.</para>
      </listitem>
    </varlistentry>

    <varlistentry>
      <term><option>-r</option>:<replaceable>classname</replaceable></term>
      <listitem>
	<para>Specify a class to be used for resolving all URIs.</para>
      </listitem>
    </varlistentry>

    <varlistentry>
      <term><option>-repeat</option>:<replaceable>N</replaceable></term>
      <listitem>
	<para>Repeat the transformation N times. For benchmarking
	  purposes.</para>
      </listitem>
    </varlistentry>

    <varlistentry>
      <term><option>-sa</option></term>
      <listitem>
	<para>Perform Schema-aware processing. Saxon-SA only.</para>
      </listitem>
    </varlistentry>

    <varlistentry>
      <term><option>-strip</option>:<group choice="req"><arg>all</arg><arg>none</arg><arg>ignorable</arg></group></term>
      <listitem>
	<para>Specify whitespace stripping policy for source documents: strip
	  all of them ("all"), strip none of them ("none"), strip ignorable
	  whitespace only ("ignorable"). Default: none.</para>
      </listitem>
    </varlistentry>

    <varlistentry>
      <term><option>-t</option></term>
      <listitem>
	<para>Display version, timing, and other debugging information on
	  standard error.</para>
      </listitem>
    </varlistentry>

    <varlistentry>
      <term><option>-tree</option>:<group choice="req"><arg>tiny</arg><arg>linked</arg></group></term>
      <listitem>
	<para>Select the implementation of the internal tree model: tiny tree
	  model ("tiny") or linked tree model ("linked"). See the Saxon
	  documentation for more information on the internal tree model.
	  Default: tiny.</para>
      </listitem>
    </varlistentry>

    <varlistentry>
      <term><option>-T</option><arg choice="opt">:<replaceable>classname</replaceable></arg></term>
      <listitem>
	<para>Trace various aspect of the processing; an optional class name can
	  be given to specify a user-chosen tracer. The class must implement
	  <classname>net.sf.saxon.trace.TraceListener</classname>. The default
	  is a system supplied tracer. This option implies
	  <option>-l</option>.</para>
      </listitem>
    </varlistentry>

    <varlistentry>
      <term><option>-TJ</option></term>
      <listitem>
	<para>Enable tracing of external Java method invocation. See
	  <option>-ext</option>.</para>
      </listitem>
    </varlistentry>

    <varlistentry>
      <term><option>-u</option></term>
      <listitem>
	<para>Force interpretation of source document names as URI. By default
	  they are considered to be file names, unless they start with "file:" or
	  "http:".</para>
      </listitem>
    </varlistentry>

    <varlistentry>
      <term><option>-val</option>:<group choice="req"><arg>strict</arg><arg>lax</arg></group></term>
      <listitem>
	<para>When validation is enabled, choose among "strict" or "lax"
	  validation.  Saxon-SA only.</para>
      </listitem>
    </varlistentry>

    <varlistentry>
      <term><option>-x</option>:<replaceable>classname</replaceable></term>
      <listitem>
	<para>Specify a class to be used as SAX parser for input documents. The
	  class must implement either <classname>org.xml.sax.Parser</classname>
	  or <classname>org.xml.sax.XMLReader</classname>.</para>
      </listitem>
    </varlistentry>

    <varlistentry>
      <term><option>-xi</option></term>
      <listitem>
	<para>Apply XInclude processing to all input documents.</para>
      </listitem>
    </varlistentry>

    <varlistentry>
      <term><option>-xmlversion</option>:<group choice="req"><arg>1.0</arg><arg>1.1</arg></group></term>
      <listitem>
	<para>Choose the XML version for processing input documents. "1.1" must
	  be specified to process XML 1.1 and Namespaces 1.1 constructs.
	  Default: 1.0.</para>
      </listitem>
    </varlistentry>

    <varlistentry>
      <term><option>-?</option></term>
      <listitem>
	<para>Display a help message and exit.</para>
      </listitem>
    </varlistentry>

  </variablelist>

</refsect2>
